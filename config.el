(setq company-idle-delay .3)

(setq doom-font (font-spec :family "Hack" :size 15 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "Roboto" :size 15))

(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

(map! :leader
      :desc "Open Org-roam UI Mode"
      "n r u" #'org-roam-ui-mode)

(map! :leader
      :desc "toggle Auto Complete"
      "t a" #'+company/toggle-auto-completion)

(map! :leader
      :desc "Switch to variable pitch mode"
      "t v" #'variable-pitch-mode)

(map! :leader
      :desc "Open Vterm"
      "o v" #'vterm)

(setq display-line-numbers-type t)

;(setq user-mail-address "zbeckr@gmail.com"
;      user-full-name "Zack Becker"
;      mu4e-get-mail-command "mbsync -C ~/.config/mu4e/mbsyncrc -a"
;      mu4e-update-interval 300
;      messege-send-mail-function 'smtpmail-send-it
;      starttls-use-gnutls t
;      smtp-starttls-credentials'(("smtp.landl.com" 587 nil nil))
;      smtpmail-auth-credentials '(("smtp.landl.com" 587 "zbeckr@gmail.com" nil))
;      smtpmail-default-smtp-server "smtp.landl.com"
;      smtpmail-smtp-server "smtp.landl.com"
;      smtpmail-smtp-service 587
;      mu4e-sent-folder "/Sent"
;      mu4e-drafts-folder "/Drafts"
;      mu4e-trash-folder "/Trash"
;      mu4e-refile-folder "/All Mail")

(setq org-directory "~/org/")
(add-hook 'org-mode-hook 'visual-line-mode)

(setq org-hide-emphasis-markers t)

(setq org-roam-capture-templates
 '(("d" "default" plain ;; the "d" is the letter you press to select, the "default is the name"
   "%?"  ;;This is where your cursor goes
   :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n") ;;this line  will basically be in every capture template
   :unnarrowed t) ;;this line also will be in basically every capture template
  ("b" "book notes" plain
   "\n* Source\n\nAuthor: %^{Author}\nTitle: ${title}\nYear: %^{Year}\n\n* Summary\n\n%?"
   :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
   :unnarrowed t)
  ("n" "notes" plain
   (file "~/org/template/roam/BasicNoteTemplate.org")
   :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
    :unnarrowed t)
    )
 )

(use-package! websocket
    :after org-roam)

(use-package! org-roam-ui
    :after org-roam ;; or :after org
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
    )
)

(setq doom-theme 'doom-dracula)
